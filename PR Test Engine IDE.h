
// PR Test Engine IDE.h : main header file for the PR Test Engine IDE application
//
#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"       // main symbols


// CPRTestEngineIDEApp:
// See PR Test Engine IDE.cpp for the implementation of this class
//

class CPRTestEngineIDEApp : public CWinApp
{
public:
	CPRTestEngineIDEApp();


// Overrides
public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();

// Implementation
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern CPRTestEngineIDEApp theApp;
