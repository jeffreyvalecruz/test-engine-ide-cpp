
// PR Test Engine IDEView.cpp : implementation of the CPRTestEngineIDEView class
//

#include "stdafx.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "PR Test Engine IDE.h"
#endif

#include "PR Test Engine IDEDoc.h"
#include "PR Test Engine IDEView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CPRTestEngineIDEView

IMPLEMENT_DYNCREATE(CPRTestEngineIDEView, CView)

BEGIN_MESSAGE_MAP(CPRTestEngineIDEView, CView)
END_MESSAGE_MAP()

// CPRTestEngineIDEView construction/destruction

CPRTestEngineIDEView::CPRTestEngineIDEView()
{
	// TODO: add construction code here

}

CPRTestEngineIDEView::~CPRTestEngineIDEView()
{
}

BOOL CPRTestEngineIDEView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CView::PreCreateWindow(cs);
}

// CPRTestEngineIDEView drawing

void CPRTestEngineIDEView::OnDraw(CDC* /*pDC*/)
{
	CPRTestEngineIDEDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: add draw code for native data here
}


// CPRTestEngineIDEView diagnostics

#ifdef _DEBUG
void CPRTestEngineIDEView::AssertValid() const
{
	CView::AssertValid();
}

void CPRTestEngineIDEView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CPRTestEngineIDEDoc* CPRTestEngineIDEView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CPRTestEngineIDEDoc)));
	return (CPRTestEngineIDEDoc*)m_pDocument;
}
#endif //_DEBUG


// CPRTestEngineIDEView message handlers
