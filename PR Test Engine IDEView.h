
// PR Test Engine IDEView.h : interface of the CPRTestEngineIDEView class
//

#pragma once


class CPRTestEngineIDEView : public CView
{
protected: // create from serialization only
	CPRTestEngineIDEView();
	DECLARE_DYNCREATE(CPRTestEngineIDEView)

// Attributes
public:
	CPRTestEngineIDEDoc* GetDocument() const;

// Operations
public:

// Overrides
public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:

// Implementation
public:
	virtual ~CPRTestEngineIDEView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in PR Test Engine IDEView.cpp
inline CPRTestEngineIDEDoc* CPRTestEngineIDEView::GetDocument() const
   { return reinterpret_cast<CPRTestEngineIDEDoc*>(m_pDocument); }
#endif

